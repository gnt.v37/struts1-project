package struts1.form;

import org.apache.struts.action.ActionForm;

public class HelloWorld extends ActionForm {

    private String greeting;

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }
}
