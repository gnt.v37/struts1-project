package struts1.action;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWorld extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

        struts1.form.HelloWorld helloWorld = (struts1.form.HelloWorld) form;

        helloWorld.setGreeting("Hello World, This is Struts Project");

        return mapping.findForward("success");
    }
}
